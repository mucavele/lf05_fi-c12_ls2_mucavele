package com.lf5_LS02.A4;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // KLINGONEN
        // ladungen -> klingonen
        Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);

        // klingonen -> Raumschiff Objekt
        Raumschiff klingonen = new Raumschiff(1, 100,
                100,
                100, 100, 2,
                "IKS Hegh'ta", new ArrayList<>(), new ArrayList<>());

        // Klingonen Ladungen werden den Klingonen Raumschiffen zugewiesen
        klingonen.addLadung(schneckensaft);
        klingonen.addLadung(schwert);

        // Ausgabe: Klingonen Objekt
        System.out.println("Klingonen:\n" + klingonen + "\n");

        // Ausgabe Ladungsverzeichnis der Klingonen
        klingonen.ladungsverzeichnisAusgeben();


        // ROMULANER

        // ladungen -> romulaner
        Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
        Ladung roteMaterie = new Ladung("Rote Materie", 2);
        Ladung plasmaWaffe = new Ladung("Plasma Waffe", 50);


        // romulaner -> Raumschiff Objekt
        Raumschiff romulaner = new Raumschiff(2, 100,
                100,
                100, 100, 2,
                "IRW Khazara", new ArrayList<>(), new ArrayList<>());

        // romulaner Ladungen werden den romulaner Raumschiffen zugewiesen
        romulaner.addLadung(borgSchrott);
        romulaner.addLadung(roteMaterie);
        romulaner.addLadung(plasmaWaffe);

        // Ausgabe: Romulaner Objekt
        System.out.println("Romulaner:\n" + romulaner);

        // Ausgabe Ladungsverzeichnis der Romulaner
        romulaner.ladungsverzeichnisAusgeben();

        // VULKANIER

        // ladungen -> vulkanier
        Ladung forschungssonde = new Ladung("Forschungssonde", 35);
        Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);


        // vulkanier -> Raumschiff Objekt
        Raumschiff vulkanier = new Raumschiff(3, 80,
                80,
                50, 100, 5,
                "Ni'Var", new ArrayList<>(), new ArrayList<>());

        // vulkanier Ladungen werden den vulkanier Raumschiffen zugewiesen
        vulkanier.addLadung(forschungssonde);
        vulkanier.addLadung(photonentorpedo);

        System.out.println(vulkanier.getLadungsverzeichnis());

        // Ausgabe des Vulkanier Objekts
        System.out.println("Vulkanier:\n" + vulkanier + "\n");

        // Ausgabe Ladungsverzeichnis der Vulkanier
        vulkanier.ladungsverzeichnisAusgeben();

        vulkanier.phaserkanoneSchiessen(vulkanier);

        // Ausgabe des Vulkanier Objekts
        System.out.println("\nNeue Werte:");
        System.out.println("Vulkanier:\n" + vulkanier + "\n");
    }
}
