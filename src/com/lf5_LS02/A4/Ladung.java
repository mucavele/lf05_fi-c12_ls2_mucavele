package com.lf5_LS02.A4;

/**
 * Die Ladung Klasse setzt due Eigenschaften einer Ladung fest.
 * Die Ladungen koennen in Raumschiff Objeten eingesetzt werden.
 * @author Kenneth Mucavele
 * @version 1.0
 */

public class Ladung {

    /**
     * Klassen Attribute
     */
    private String bezeichnung;
    private int menge;

    public Ladung() {}

    /**
     * Voll parametrisierter Konstruktor
     * @param bezeichnung Bezeichnung der Ladung
     * @param menge Menge der Ladung
     */
    public Ladung(String bezeichnung, int menge){
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    /**
     * Weist der Ladungsbezeichnung einen neuen Wert zu
     * @param newBezeichnung Neue Ladungsbezeichnung
     */
    public void setBezeichnung(String newBezeichnung){
        this.bezeichnung = newBezeichnung;
    }

    /**
     * Gibt die Ladungsbezeichnung zurueck
     * @return String
     */
    public String getBezeichnung(){
        return bezeichnung;
    }

    /**
     * Weist der Ladungsmenge einen neuen Wert zu
     * @param newMenge Neue Ladungsmenge
     */
    public void setMenge(int newMenge){
        this.menge = newMenge;
    }

    /**
     * Gibt die Ladungsmenge zurueck
     * @return String
     */
    public int getMenge(){
        return menge;
    }

    /**
     * Gibt das Ladungsobjekt zurueck
     * @return String
     */
    @Override
    public String toString() {
        return "\nLadung:\n" +
                "Bezeichnung: " + bezeichnung + '\n' +
                "Menge: " + menge + '\n';
    }
}
