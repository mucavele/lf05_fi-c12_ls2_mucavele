package com.lf5_LS02.A4;

import java.util.ArrayList;


/**
 * Die Ladung Klasse setzt due Eigenschaften einer Ladung fest.
 * Die Ladungen koennen in Raumschiff Objeten eingesetzt werden.
 * @author Kenneth Mucavele
 * @version 1.0
 */


public class Raumschiff {
    /**
     * Klassen Attribute
     */

    // attribute
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private ArrayList<String> broadcastKommunikator;
    private ArrayList<Ladung> ladungsverzeichnis;

    public Raumschiff() {}

    /**
     * Voll parametrisierter Konstruktor
     * @param photonentorpedoAnzahl Anzahl der Photonentorpedos auf dem Raumschiff
     * @param energieversorgungInProzent Energieversorgung des Raumschiffs in Prozent
     * @param schildeInProzent Zustand des Schildschutzes in Prozent
     * @param huelleInProzent Zustand des Huellenschutzes in Prozent
     * @param lebenserhaltungssystemeInProzent Zustand der Lebenserhaltungssysteme in Prozent
     * @param androidenAnzahl Anzahl der Androiden auf dem Raumschiff
     * @param schiffsname Name des Raumschiffs
     * @param broadcastKommunikator Nachrichten im BroadcastKommunikators des Raumschiffs
     * @param ladungsverzeichnis Verzeichnis der an Bord vorhandenen Ladungen
     */

    // "voller" Konstruktor
    public Raumschiff(
            int photonentorpedoAnzahl, int energieversorgungInProzent,
            int schildeInProzent, int huelleInProzent,
            int lebenserhaltungssystemeInProzent, int androidenAnzahl,
            String schiffsname,
            ArrayList<String> broadcastKommunikator,
            ArrayList<Ladung> ladungsverzeichnis)
    {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
        this.broadcastKommunikator = broadcastKommunikator;
        this.ladungsverzeichnis = ladungsverzeichnis;
    }


    /**
     * Gibt die Anzahl der Photonentorpedos zurueck
     * @return int
     */
    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }


    /**
     * Weist der Photonentorpedo anzahl einen neuen Wert zu
     * @param photonentorpedoAnzahl Anzahl der Photonentorpedos
     */
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    /**
     * Gibt die Energieversorgung in Prozent zurueck
     * @return int
     */
    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    /**
     * Weist der Energieversorgung einen neuen Wert zu
     * @param energieversorgungInProzent Energieversorgung in Prozent
     */
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    /**
     * Gibt den Zustand des Schildschutzes in Prozent zurueck
     * @return int
     */
    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    /**
     * Weist der Schildschutz einen neuen Wert zu
     * @param schildeInProzent  Zustand des Schildschutzes in Prozent
     */
    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    /**
     * Gibt den Zustand des Huellenschutzes in Prozent zurueck
     * @return int
     */
    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    /**
     * Weist den Zustand des Huellenschutzes einen neuen Wert zu
     * @param huelleInProzent  Zustand des Huellenschutzes in Prozent
     */
    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    /**
     * Gibt den Zustand der Lebenserhaltungssysteme in Prozent zurueck
     * @return int
     */
    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    /**
     * Weist den Zustand der Lebenserhaltungssysteme einen neuen Wert zu
     * @param lebenserhaltungssystemeInProzent Lebenserhaltungssysteme in Prozent
     */
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }


    /**
     * Gibt die Androiden Anzahl zurueck
     * @return int
     */
    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    /**
     * Weist der Androiden Anzahl einen neuen Wert zu
     * @param androidenAnzahl Anzahl der Androiden auf dem Raumschiff
     */
    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    /**
     * Gibt den Schiffsnamen zurueck
     * @return String
     */
    public String getSchiffsname() {
        return schiffsname;
    }

    /**
     * Weist dem Schiffsnamen einen neuen Wert zu
     * @param schiffsname Name des Raumschiffs
     */
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }


    /**
     * Gibt eine Liste der Nachrichten im BroadcastKommunikators zurueck
     * @return ArrayList
     */
    public ArrayList<String> getLogbuchEintraege() {
        return broadcastKommunikator;
    }

    /**
     * Weist dem BroadcastKommunikator eine Liste mit Nachrichten zu
     * @param broadcastKommunikator Liste der Nachrichten im BroadcastKommunikators
     */
    public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
        this.broadcastKommunikator = broadcastKommunikator;
    }

    /**
     * Gibt eine Liste der Ladungen auf dem Raumschiff zurueck
     * @return ArrayList
     */
    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    /**
     * Weist dem Ladungsverzeichnis eine Liste mit Ladungen zu
     * @param ladungsverzeichnis Liste der an Bord vorhandenen Ladungen
     */
    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    /**
     * Fuegt dem Raumschiff eine Ladung hinzu
     * @param neueLadung Ladungsobjekt
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    /**
     * Schiesst Photonentorpedos ab.
     * Beim abschuss wird die Anzahl der Photonentorpedo um 1 dekrementiert und
     * die Nachricht "Photonentorpedo auf Raumschiffsname abgeschossen" wird ausgegeben.
     * Falls keine Photonentorpedos an Bord sind, wird an alle die Nachricht "-=*Click*=-" gesendet.
     * Die treffer() Methode wird mit dem Parameter Raumschiff Objekt aufgerufen.
     * @param raumschiff Ein Raumschiff Objekt
     */
    public void photonentorpedoSchiessen(Raumschiff raumschiff) {
        treffer(raumschiff);
        if (photonentorpedoAnzahl == 0){
            nachrichtAnAlle("-=*Click*=-");
            return;
        } else{
            setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
        }
        nachrichtAnAlle("Photonentorpedo auf " + raumschiff.getSchiffsname() + " abgeschossen");
    }

    /**
     * Schiesst Phaserkanonen ab.
     * Beim abschuss wird die Energieversorgung um 50 dekrementiert und
     * die Nachricht "Phaserkanone auf <Raumschiffsname> abgeschossen" wird ausgegeben.
     * Falls die Energieversorgung unter 50 Prozent ist, wird an alle die Nachricht "-=*Click*=-" gesendet.
     * Die treffer() Methode wird mit dem Parameter Raumschiff Objekt aufgerufen.
     * @param raumschiff Ein Raumschiff Objekt
     */
    public void phaserkanoneSchiessen(Raumschiff raumschiff) {
        if(getEnergieversorgungInProzent() < 50){
            nachrichtAnAlle("-=*Click*=-");
        } else {
            setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
            nachrichtAnAlle("Phaserkanone auf " + raumschiff.getSchiffsname() + " abgeschossen");
        }
        treffer(raumschiff);
    }

    /**
     * Folgende Methoden werden fuer das eingabe Raumschiff aufgerufen:
     *      Die setSchildeInProzent() Methoden vermindert den Zustand des Schutzschildes um 50 Prozent
     *      "<raumschiffsname> wurde getroffen wird ausgegeben
     *      Wenn der Zustand des Schutzschildes gleich 0 ist, 
     *          werden die Energieversorgung und der Zustand der Huelle um 50 Prozent vermindert
     *      Wenn der Zustand der Huelle anschliessend gleich 0 ist,
     *          wird die Nachricht "Alle Lebenserhaltungssysteme von " + <Raumschiffsname> + " vernichtet!" an alle ausgegeben.
     * @param raumschiff Eingabe Raumschiff Objekt
     */
    private void treffer(Raumschiff raumschiff) {
        raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent() - 50);
        System.out.println(raumschiff.getSchiffsname() + " wurde getroffen!");
        if (raumschiff.getSchildeInProzent() == 0){

            raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);
            raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent() - 50);

            if (getHuelleInProzent() == 0) {
                nachrichtAnAlle("Alle Lebenserhaltungssysteme von " + raumschiff.getSchiffsname() + " vernichtet!");
            }
        }
    }

    /**
     * Methode fuegt die Nachricht dem Broadcast Kommunikator hinzu.
     * @param message Nachricht
     */
    public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(message);
    }

    public ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return new ArrayList<>();
    }

    public void photonentorpedosLaden(int anzahlTorpedos) {
        if (getPhotonentorpedoAnzahl() == 0){
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        }
    }


    /**
     * Gibt den Zustand des Raumschiffes aus
     */
    public void zustandRaumschiff() {
        System.out.printf("""
                Zustand für Raumschiff= %s
                Photonentorpedo Anzahl= %s
                Energieversorgung in Prozent= %s
                SchildeInProzent= %s
                Huelle in Prozent= %s
                Lebenserhaltungssysteme in Prozent= %s
                Androiden Anzahl= %s
                Schiffsname= %s
                Broadcast Kommunikator= %s
                Ladungsverzeichnis= %s
                """,
                schiffsname, photonentorpedoAnzahl, energieversorgungInProzent, schildeInProzent,
                huelleInProzent, lebenserhaltungssystemeInProzent, androidenAnzahl, schiffsname,
                broadcastKommunikator, ladungsverzeichnis);
    }

    /**
     * Gibt die Ladungen im Ladungsverzeichnis aus
     */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungsverzeichnis von Raumschiff: " + getSchiffsname());
        for (Ladung ladung : ladungsverzeichnis){
            System.out.println(ladung.toString());
        }
    }


    /**
     * Gibt alle Daten des Raumschiffs aus
     * @return String Objektausgabe
     */
    @Override
    public String toString() {
        return "Raumschiff - " + schiffsname + '\n' +
                " photonentorpedoAnzahl=" + photonentorpedoAnzahl +
                ",\n energieversorgungInProzent=" + energieversorgungInProzent +
                ",\n schildeInProzent=" + schildeInProzent +
                ",\n huelleInProzent=" + huelleInProzent +
                ",\n lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent +
                ",\n androidenAnzahl=" + androidenAnzahl +
                ",\n schiffsname='" + schiffsname + '\'' +
                ",\n broadcastKommunikator=" + broadcastKommunikator +
                ",\n ladungsverzeichnis=" + ladungsverzeichnis;
    }
}
